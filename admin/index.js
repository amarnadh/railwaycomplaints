const express = require("express");
const app = express();
const mongoose = require("mongoose");
const complaints = require('./models/complaint.js')
mongoose
  .connect("mongodb://127.0.0.1:27017/railway")
  .then((e) => console.log("MongoDB is ready and connected"))
  .catch(console.error);
const imageSchema = require('./models/image.js')
app.set("view engine", "ejs");
app.get("/",(req,res) =>
    {
        res.render('login')
    })
app.get("/menus",async (req, res) => {
    try {
        const products = await complaints.find();
      console.log(products)
      res.render("menus", { data: products });
    
    } catch (error) {
        res.status(500).json({ error: 'Internal server error' });
    }

});

app.get("/view/:id",async (req,res) =>
  {
    let imgData = await imageSchema.findOne({cid:req.params.id})
  res.render('view',{imginfo:imgData})

  })

app.get("/test",async (req,res) =>
{ try {
    const products = await complaints.find();

  res.render("menus", { data: products });
  
} catch (error) {
    res.status(500).json({ error: 'Internal server error' });
}

})
app.listen(8000);
