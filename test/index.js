const express = require('express');
const fileUpload = require('express-fileupload');
const app = express();
const port = 3000;

// Use the express-fileupload middleware
app.use(fileUpload());

app.use(express.static('public'));

app.get('/', (req, res) => {
    res.send('Hello World! change........................');
});

app.post('/upload', (req, res) => {
    // Log the files to the console
    console.log(req.files);

    // All good
    res.sendStatus(200);
});

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
});