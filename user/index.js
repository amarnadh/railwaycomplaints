
const express = require("express");
const { type } = require("express/lib/response");
const bodyparser = require("body-parser");
const favion = require('serve-favicon')
const multer = require('multer')
const fs = require('fs')
const imageSchema = require("./model/image.js")
const app = express();
app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());

app.set("view engine", "ejs");
const complaints = require("./model/complaint.js");
const mongoose = require("mongoose");
const path = require('path');
const uuid = require('uuid')
const { log } = require("console");
app.use(express.json());
app.use("/public", express.static('public')); 
mongoose
  .connect("mongodb://127.0.0.1:27017/railway")
  .then((e) => console.log("MongoDB is ready and connected"))
  .catch(console.error);

  const storageEngine = multer.diskStorage({
    destination: "./images",
    filename: (req, file, cb) => {
      cb(null, `${Date.now()}--${file.originalname}`);
    },
  });
  const upload = multer({
    storage: storageEngine,
  });
  app.use(favion(path.join(__dirname,'./public/','favicon.ico')))
app.get("/get", (req, res) => {
  res.render("i");
});

app.post("/post",upload.single("image"),(req, res) => {
    
  const uid = uuid.v4()
    
   let date_time = new Date();
  const newComplaints = new complaints({cid:uid,
    name: req.body.name,
    complaint: req.body.complaint,
    date: date_time,
    train: req.body.trainnumber,
    coachnumber: req.body.coachnumber,
  });
const newImage = new imageSchema({cid:uid,
name:"image",des:"no des",img:{data:fs.readFileSync(path.join(__dirname+'/images/'+req.file.filename)),contentType:"jpg"}

 })
newImage.save()


  newComplaints.save();
  
  res.redirect("/submited");

});
app.get("/submited", (req, res) => {
  res.render("submited");
});
app.get("/view",async (req,res) =>
{
  let imgData = await imageSchema.findOne({}).sort({_id: -1}).limit(1)
  res.render('test',{imginfo:imgData})
  
})
app.listen(3000, () => {
  console.log("port is running at 3000");
});
