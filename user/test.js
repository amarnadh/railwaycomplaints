const express = require('express');
const app = express();
const multer = require('multer')
const port = process.env.PORT || 3000;
app.set("view engine", "ejs");
const storageEngine = multer.diskStorage({
    destination: "./images",
    filename: (req, file, cb) => {
      cb(null, `${Date.now()}--${file.originalname}`);
    },
  });
  const upload = multer({
    storage: storageEngine,
  });
  app.get("/",(req,res) =>
{
    res.render("test")
})  
  app.post("/single", upload.single("image"), (req, res) => {
    if (req.file) {
      res.send("Single file uploaded successfully");
      console.log(req.file);
    } else {
      res.status(400).send("Please upload a valid image");
    }
  });
  
app.listen(port, ()=>{
    console.log(`App is listening on port ${port}`);
});